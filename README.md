# DimaBank

### REST API

Create client with `$login`:

    curl -d '"$login"' -H "Content-Type: application/json" -X POST localhost:8000/create-client

Create account with `$clientId`:

    curl -d '"$clientId"' -H "Content-Type: application/json" -X POST localhost:8000/create-account

Add `$amount` to account with `$accountId`:

    curl -d '$amount' -H "Content-Type: application/json" -X POST localhost:8000/add-money/$clientId

Transfer `$amount` from account `$senderId` to account `$recipientId`:

    curl -d '{"senderId":"$senderId","recipientId":"$recipientId","amount":$amount}' -H "Content-Type: application/json" -X POST localhost:8000/transfer

List all clients (in browser `http://localhost:8000/clients`):

    curl -X GET localhost:8000/clients
    
List all accounts (in browser `http://localhost:8000/accounts`):

    curl -X GET localhost:8000/accounts
    
List all transfers or transfers with `$accountId` (in browser `http://localhost:8000/transfers`):

    curl -X GET localhost:8000/transfers
    curl -X GET localhost:8000/transfers/$accoundId
    
## Packaging 

Create fatjar with 

    sbt assembly

Run DimaBank with docker:

    sudo docker image build -t dimabank_image .
    sudo docker run --name dimabank -p:8000:8000 -e POSTGRES_USER=dimabank -e POSTGRES_PASSWORD=dimabank -e POSTGRES_DB=dimabank dimabank_image -d postgres
    
## DB Schema

"CLIENTS"

                                            Table "public.CLIENTS"
      Column  |       Type        | Collation | Nullable | Default | Storage  | Stats target | Description 
    ----------+-------------------+-----------+----------+---------+----------+--------------+-------------
     CLIENTID | uuid              |           | not null |         | plain    |              | 
     LOGIN    | character varying |           | not null |         | extended |              | 
    Indexes:
        "CLIENTS_pkey" PRIMARY KEY, btree ("CLIENTID")
        "CLIENTS_LOGIN_key" UNIQUE CONSTRAINT, btree ("LOGIN")


"ACCOUNTS"

                                      Table "public.ACCOUNTS"
       Column   |     Type      | Collation | Nullable | Default | Storage | Stats target | Description 
     -----------+---------------+-----------+----------+---------+---------+--------------+-------------
      CLIENTID  | uuid          |           | not null |         | plain   |              | 
      ACCOUNTID | uuid          |           | not null |         | plain   |              | 
      BALANCE   | numeric(21,2) |           | not null |         | main    |              | 
    Indexes:
        "ACCOUNTS_pkey" PRIMARY KEY, btree ("ACCOUNTID")

"TRANSFERS"

                                             Table "public.TRANSFERS"
       Column    |       Type        | Collation | Nullable | Default | Storage  | Stats target | Description 
    -------------+-------------------+-----------+----------+---------+----------+--------------+-------------
     TRANSFERID  | uuid              |           | not null |         | plain    |              | 
     TIME        | character varying |           | not null |         | extended |              | 
     SENDERID    | uuid              |           | not null |         | plain    |              | 
     RECEPIENTID | uuid              |           | not null |         | plain    |              | 
     AMOUNT      | numeric(21,2)     |           | not null |         | main     |              | 
    Indexes:
        "TRANSFERS_pkey" PRIMARY KEY, btree ("TRANSFERID")