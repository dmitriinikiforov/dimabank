name := "DimaBank"

version := "0.1"

scalaVersion := "2.13.1"

organization := "com.gitlab.dmitriinikiforov"

libraryDependencies ++= Seq(
  "com.typesafe.slick" %% "slick" % "3.3.2",
  "org.postgresql" % "postgresql" % "42.2.8",
  "com.typesafe.akka" %% "akka-actor" % "2.5.26",
  "com.typesafe.akka" %% "akka-stream" % "2.5.26",
  "com.typesafe.akka" %% "akka-http" % "10.1.10",
  "de.heikoseeberger" %% "akka-http-json4s" % "1.29.1",
  "com.typesafe.slick" %% "slick" % "3.3.2",
  "org.json4s" %% "json4s-jackson" % "3.6.7",
  "org.json4s" %% "json4s-core" % "3.6.7",
  "org.scalatest" %% "scalatest" % "3.0.8" % "test"
)

enablePlugins(DockerPlugin)

dockerBaseImage := "openjdk:8-slim"
packageName in Docker := "dmitriinikiforov.gitlab.com/DimaBank"
dockerExposedPorts ++= Seq(8080)
dockerUpdateLatest := true
